package SoringAlgorithms;

import java.util.Arrays;

/**
 * Code For Bubble Sort
 */
public class BubbleSort {
    public static void main(String[] args) {
        int[] arr = {1,50,-1,23,6};
        System.out.println(Arrays.toString(bubbleSort(arr)));
    }

    public static int[] bubbleSort(int[] arr) {
        for(int i= arr.length-1; i>0; i--){
            for(int j=0; j<i;j++){
                if(arr[j]> arr[j+1]){
                    swap(arr, j, j+1);
                }
            }
        }
        return arr;
    }

    private static void swap(int[] arr, int i, int j) {
        int temp;
        temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
