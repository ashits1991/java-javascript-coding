package leetcode.Easy.removeelement.java;

import java.util.Arrays;

/**
 * [3,2,2,3] , 3.
 */
public class RemoveElement {
    public int removeElement(int[] A, int elem){
        int validSize =0;
        if(A.length==0) return 0;
        for(int i=0; i< A.length; i++){
           if(A[i]!= elem){
               A[validSize++] = A[i];
           }
        }
        System.out.println(Arrays.toString(A));
        return validSize;
    }
}
