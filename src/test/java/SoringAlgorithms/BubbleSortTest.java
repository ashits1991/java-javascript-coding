package SoringAlgorithms;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BubbleSortTest {
    BubbleSort bb;
    @BeforeEach
    void setUp() {
        bb = new BubbleSort();
    }

    @Test
    void bubbleSort() {
        assertArrayEquals(new int[]{-1, 1, 6, 23, 50}, bb.bubbleSort(new int[]{1,50,-1,23,6}));
    }
}