package leetcode.Easy.Java;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ImplementstrStrTest {

    ImplementstrStr implementstrStr;
    @BeforeEach
    void setUp() {
        implementstrStr = new ImplementstrStr();
    }

    @Test
    void strStr() {
        assertAll( "Testing Different Test Cases For This Class",
                ()->   assertEquals(2, implementstrStr.strStr("hello","ll")),
                ()->  assertNotEquals(3, implementstrStr.strStr("hello","ll")),
                ()->  assertEquals(-1, implementstrStr.strStr("aaaaa",  "bba")),
                () -> assertEquals(0, implementstrStr.strStr("","")),
                () -> assertEquals(-1, implementstrStr.strStr("","a"))
        );
    }
}