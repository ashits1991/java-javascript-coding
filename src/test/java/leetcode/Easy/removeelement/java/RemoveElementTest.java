package leetcode.Easy.removeelement.java;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by ashitsathish on May,2020
 */
class RemoveElementTest {
    RemoveElement removeElement;
    @BeforeEach
    void setUp() {
        removeElement = new RemoveElement();
    }

    @Test
    void test1() {
        assertEquals(2, removeElement.removeElement(new int[]{3,2,2,3},3));
        assertEquals(5, removeElement.removeElement(new int[]{0,1,2,2,3,0,4,2},2));
    }
}